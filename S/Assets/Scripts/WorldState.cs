using UnityEngine;
using System.Collections;

public static class WorldState {

	public static int streamsSolved = 0;
	public static bool teleported = false;
	public static bool gameOver = false;
	
	public static bool specificSolved = false;
	public static bool pressureSolved = false;
	public static bool throwSolved = false;
	public static bool stackSolved = false;
}
